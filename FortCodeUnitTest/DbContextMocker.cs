﻿using System;
using System.Collections.Generic;
using System.Text;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCodeUnitTest
{
    public static class DbContextMocker
    {
        public static TestDBContext GetTestDBContext(string dbName)
        {
            // Create options for DbContext instance
            var options = new DbContextOptionsBuilder<TestDBContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;

            // Create instance of DbContext
            var dbContext = new TestDBContext(options);

            // Add entities in memory
            dbContext.Seed();

            return dbContext;
        }
    }
}
