﻿using System;
using System.Collections.Generic;
using System.Text;
using FortCode.Models;

namespace FortCodeUnitTest
{
    public static class DbContextExtensions
    {
        public static void Seed(this TestDBContext dBContext)
        {
            dBContext.Cities.Add(new City
            {
                CityName = "Hyderabad",
                Country = "India",
                UserId = 1
            });

            dBContext.Cities.Add(new City
            {
                CityName = "Vijayawada",
                Country = "India",
                UserId = 1
            });

            dBContext.Cities.Add(new City
            {
                CityName = "Chennai",
                Country = "India",
                UserId = 1
            });

            dBContext.Cities.Add(new City
            {
                CityName = "Hyderabad",
                Country = "India",
                UserId = 2
            });

            dBContext.Cities.Add(new City
            {
                CityName = "Vijayawada",
                Country = "India",
                UserId = 2
            });

            dBContext.Cities.Add(new City
            {
                CityName = "Chennai",
                Country = "India",
                UserId = 2
            });

            dBContext.Cities.Add(new City
            {
                CityName = "Hyderabad",
                Country = "India",
                UserId = 3
            });

            dBContext.Cities.Add(new City
            {
                CityName = "Vijayawada",
                Country = "India",
                UserId = 3
            });

            dBContext.Cities.Add(new City
            {
                CityName = "Chennai",
                Country = "India",
                UserId = 3
            });

            dBContext.UserAccounts.Add(new UserAccount
            {
                Id =1,
                Name = "ABC",
                Email = "abc@gmail.com",
                Password = "test"
            });

            dBContext.UserAccounts.Add(new UserAccount
            {
                Id = 2,
                Name = "ABCD",
                Email = "abcd@gmail.com",
                Password = "test1"
            });

            dBContext.UserAccounts.Add(new UserAccount
            {
                Id = 3,
                Name = "ABCDE",
                Email = "abcde@gmail.com",
                Password = "test2"
            });
        }
    }
}
