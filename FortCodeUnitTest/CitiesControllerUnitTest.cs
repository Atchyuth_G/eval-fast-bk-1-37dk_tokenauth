﻿using FortCode.Controller;
using FortCode.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Xunit;

namespace FortCodeUnitTest
{
    public class CitiesControllerUnitTest
    {
        [Fact]
        public void TestGetFavouriteCities()
        {       
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]{
            new Claim(ClaimTypes.Name, "1"),
        }, "mock"));
            var dbContext = DbContextMocker.GetTestDBContext(nameof(TestGetFavouriteCities));
            var controller = new CitiesController(dbContext);
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            var response = controller.GetFavouriteCities();
            var value = response.Value;
            var result = new FavCityViewModel { CityName = "Hyderabad"};
            dbContext.Dispose();

            Assert.Equal(result,value);
        }

        [Fact]
        public void TestAddFavouriteCity()
        {            
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]{
            new Claim(ClaimTypes.Name, "1"),
        }, "mock"));
            var dbContext = DbContextMocker.GetTestDBContext(nameof(TestAddFavouriteCity));
            var controller = new CitiesController(dbContext);
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            var response = controller.AddFavouriteCity("Banglore","India");                      
            dbContext.Dispose();

            Assert.Equal("Success", response);
        }

        [Fact]
        public void TestDeleteFavouriteCity()
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]{
            new Claim(ClaimTypes.Name, "1"),
        }, "mock"));
            var dbContext = DbContextMocker.GetTestDBContext(nameof(TestDeleteFavouriteCity));
            var controller = new CitiesController(dbContext);
            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
            var response = controller.DeleteCity("Banglore", "India");
            dbContext.Dispose();

            Assert.Equal("Success", response);
        }
    }
}
