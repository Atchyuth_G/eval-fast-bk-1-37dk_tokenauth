﻿using FortCode.Models;

namespace FortCode.Services
{
    public interface ILoginService
    {
        UserAccount Authenticate(string userName, string pwd);        
    }
}
