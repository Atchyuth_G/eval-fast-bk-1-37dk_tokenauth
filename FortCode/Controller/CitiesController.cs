﻿using FortCode.Models;
using FortCode.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using System.Linq;
using System;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Collections.Generic;

namespace FortCode.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class CitiesController : ControllerBase
    {
        private readonly TestDBContext testDBContext;

        public CitiesController(TestDBContext _testDBContext)
        {
            testDBContext = _testDBContext;
        }

        /// <summary>
        /// Get favourite cities of authenticated user
        /// </summary>
        /// <returns></returns>
        // GET: api/<CitiesController>
        [HttpGet]
        public ActionResult<FavCityViewModel> GetFavouriteCities()
        {
            int userId = GetUserId();
            var favCities = testDBContext.Cities.Where(c => c.UserId == userId);            
            if (favCities == null)
            {
                return NoContent();
            }
            return Ok(favCities.Select(w => new FavCityViewModel { CityName = w.CityName }).ToList());
        }

        /// <summary>
        /// Add favourite city of authenticated user
        /// </summary>
        /// <param name="city"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        // POST api/<CitiesController>
        [HttpPost("{city}/{country}")]
        public ActionResult<string> AddFavouriteCity(string city, string country)
        {
            int userId = GetUserId();
            City cityFound = testDBContext.Cities.FirstOrDefault(c => (c.CityName.Equals(city)) && (c.Country.Equals(country)) && (c.UserId == userId));
            if (cityFound == null)
            {
                testDBContext.Cities.Add(new City() { CityName = city, Country = country, UserId = userId });
                testDBContext.SaveChanges();
                return Ok("Success");
            }
            return BadRequest("City is already added as favourite");
        }

        /// <summary>
        /// Delete favourite city of authenticated user
        /// </summary>
        /// <param name="city"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        // DELETE api/<CitiesController>/5
        [HttpDelete("{city}/{country}")]
        public ActionResult<string> DeleteCity(string city, string country)
        {
            int userId = GetUserId();
            City cityToDel = testDBContext.Cities.FirstOrDefault(c => (c.CityName.Equals(city)) && (c.Country.Equals(country)) && (c.UserId == userId));
            if (!(cityToDel == null))
            {
                testDBContext.Cities.Remove(cityToDel);
                testDBContext.SaveChanges();
                return Ok("success");
            }
            return BadRequest("City not found to delete");
        }

        /// <summary>
        /// To get user id from email obtained from jwt token
        /// </summary>
        /// <returns></returns>
        private int GetUserId()
        {
            int userId = -1;
            if (User.Identity.IsAuthenticated)
            {
                var identity = User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims;
                    var userEmail = claims.Where(p => p.Type == "Email").FirstOrDefault().Value;
                    userId = testDBContext.UserAccounts.Where(c => c.Email == userEmail).FirstOrDefault().Id;
                }
            }
            return userId;
        }
    }
}
