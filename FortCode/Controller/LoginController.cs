﻿using FortCode.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace FortCode.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public IConfiguration _configuration;
        private readonly TestDBContext testDBContext;

        public LoginController(TestDBContext _testDBContext, IConfiguration configuration)
        {
            testDBContext = _testDBContext;
            _configuration = configuration;
        }

        /// <summary>
        /// Authenticate user based on email and password and generate a jwt token to validate
        /// </summary>
        /// <param name="user1"></param>
        /// <returns></returns>
        // GET: api/<CitiesController>
        [HttpGet]
        public ActionResult Login([FromBody] UserAccount user1)
        {                        
            if (user1.Email != null && user1.Password != null)
            {                
                UserAccount userAccount = testDBContext.UserAccounts.FirstOrDefault(c => (c.Email == user1.Email) && (c.Password == user1.Password));
                if (userAccount == null)
                {
                    return BadRequest("Invalid Credentials");
                }                                   
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["jwt:key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);                    
                var permClaims = new List<Claim>();
                permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                permClaims.Add(new Claim("Email", userAccount.Email));                
                var issuer = _configuration["jwt:issuer"];
                var audience = _configuration["jwt:issuer"];
                var token = new JwtSecurityToken(issuer: issuer,audience: audience,claims: permClaims, expires: DateTime.UtcNow.AddMinutes(10), signingCredentials: credentials);
                return Ok(new JwtSecurityTokenHandler().WriteToken(token));                                                    
            }
            else
            {
                Response.Cookies.Append("Email", string.Empty);
                return BadRequest();
            }


        }        
    }
}
