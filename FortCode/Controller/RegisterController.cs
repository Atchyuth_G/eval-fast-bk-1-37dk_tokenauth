﻿using FortCode.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace FortCode.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        public IConfiguration _configuration;
        private TestDBContext dbContext;
        public RegisterController(TestDBContext _testDBContext, IConfiguration config)
        {
            _configuration = config;
            dbContext = _testDBContext;
        }

        /// <summary>
        /// Register user with username and email
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        // POST api/<UserController>
        [HttpPost]
        public ActionResult RegisterUser([FromBody] UserAccount user)
        {
            if (dbContext.UserAccounts.FirstOrDefault(c => c.Email == user.Email) == null)
            {
                dbContext.UserAccounts.Add(user);
                dbContext.SaveChanges();
                Response.Cookies.Append("Email", string.Empty);
                return Ok("Success");
            }
            return BadRequest("User already exists");
        }
    }
}
