######Please note that this is my first project in .net core.######

#UnitTesting
Unit testing is not fully completed because of some difficulties in creating in memory database( I am using SQL Server to store details)

#API Documentation
I have very minimal experience in API documentation. So, could not complete that

#To run api
1. Create a database with name "TestDB" in SQL Server and create "UserAccount" table and "City" table
2. UserAccount table should contain following fields
	-> Id(Primary key)
	-> Email(Unique and not null)
	-> Password
3. City table should contain following fields
	-> Id(Primary Key)
	-> CityName(Not null)
	-> Country(Not null)
	-> UserId(Foreign key)
2. Modify connection string in appsettings.json
3. Install dependencies
4. Run