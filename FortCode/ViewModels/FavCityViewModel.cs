﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.ViewModels
{
    public class FavCityViewModel
    {
        public string? CityName { get; set; }
    }
}
