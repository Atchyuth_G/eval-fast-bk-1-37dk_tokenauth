﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FortCode.Models
{
    public partial class UserAccount
    {
        public UserAccount()
        {
            Cities = new HashSet<City>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }
}
